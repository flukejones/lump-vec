pub use crate::linked_iterator::{IterLinks, IterLinksMut};
use std::alloc::{alloc_zeroed, Layout};
use std::mem;
use std::mem::{align_of, size_of};
use std::ptr::NonNull;

/// Allocates a lump of memory of `capacity` size for `capacity * Option<T>`.
///
/// See `Lump` for more information on how the backing memory works.
pub struct Lump<T> {
    /// The main AllocPool buffer
    pub(crate) buf_ptr: NonNull<Option<T>>,
    /// Max capacity of the buffer
    pub(crate) capacity: usize,
    /// Actual used AllocPool
    pub(crate) len: usize,
    /// The next free slot to insert in
    next_free: usize,
}

impl<T> Lump<T> {
    /// Make a new `LumpOpt` where objects are wrapped in linked-list nodes. The `capacity`
    /// is the total amount of objects you can push - once this limit is reached
    /// a push will return `None` until you remove items.
    ///
    /// There is no method of reallocation to increase size.
    pub fn new(capacity: usize) -> Self {
        unsafe {
            let size = capacity * size_of::<Option<T>>();
            let layout = Layout::from_size_align_unchecked(size, align_of::<Option<T>>());
            let buf_ptr = alloc_zeroed(layout);

            Self {
                buf_ptr: NonNull::new_unchecked(buf_ptr as *mut _),
                capacity,
                len: 0,
                next_free: 0,
            }
        }
    }

    /// Get the actual used length of this `LumpOpt`. This is the amount of items using
    /// space in the `LumpOpt`.
    pub const fn len(&self) -> usize {
        self.len
    }

    pub const fn is_empty(&self) -> bool {
        self.len == 0
    }

    /// Get the allocated capacity of this `LumpOpt`. This is how many items you can
    /// add in toatl.
    pub const fn capacity(&self) -> usize {
        self.capacity
    }

    /// Get a reference to the item at this index. Returns `None` if there is no
    /// item at the index.
    pub fn get(&self, idx: usize) -> Option<&T> {
        debug_assert!(idx < self.capacity);
        unsafe { (*self.buf_ptr.as_ptr().add(idx)).as_ref() }
    }

    /// Get a mutable reference to the item at this index. Returns `None` if there is no
    /// item at the index.
    pub fn get_mut(&mut self, idx: usize) -> Option<&mut T> {
        debug_assert!(idx < self.capacity);
        unsafe { (*self.buf_ptr.as_ptr().add(idx)).as_mut() }
    }

    /// Push an item to the `LumpOpt`. Returns the index the item was pushed to if
    /// successful. This index can be used to remove the item, if you want to
    /// accurately remove the pushed item you should store this somewhere.
    pub fn push(&mut self, obj: T) -> Option<usize> {
        if self.len == self.capacity {
            return None;
        }

        let mut idx = self.next_free;
        unsafe {
            let ptr = self.buf_ptr.as_ptr().add(idx);
            // Check if it's empty, if not, try to find a free slot
            if (*ptr).is_some() {
                if let Some(slot) = self.find_first_free(self.next_free) {
                    idx = slot;
                } else {
                    return None;
                }
            }

            let ptr = self.buf_ptr.as_ptr().add(idx);
            std::ptr::write(ptr, Some(obj));
        }

        self.len += 1;
        if self.next_free < self.capacity - 1 {
            self.next_free += 1;
        }

        Some(idx)
    }

    /// Take `T` from the buffer, replacing with `None`
    pub fn take(&mut self, idx: usize) -> Option<T> {
        debug_assert!(idx < self.capacity);
        let tmp;
        unsafe {
            let ptr = self.buf_ptr.as_ptr().add(idx);
            tmp = ptr.read();

            if mem::needs_drop::<T>() {
                std::ptr::drop_in_place(ptr);
            }

            std::ptr::write(self.buf_ptr.as_ptr().add(idx), None);
        }

        self.len -= 1;
        self.next_free = idx; // reuse the slot on next insert

        tmp
    }

    /// Removes the entry at index
    pub fn remove(&mut self, idx: usize) {
        unsafe {
            let ptr = self.buf_ptr.as_ptr().add(idx);
            if mem::needs_drop::<T>() {
                std::ptr::drop_in_place(ptr);
            }
            std::ptr::write(self.buf_ptr.as_ptr().add(idx), None);
        }

        self.len -= 1;
        self.next_free = idx; // reuse the slot on next insert
    }

    pub(crate) fn find_first_free(&self, start: usize) -> Option<usize> {
        if self.len >= self.capacity {
            return None;
        }

        let mut ptr = unsafe { self.buf_ptr.as_ptr().add(start) };
        for idx in start..self.capacity {
            unsafe {
                ptr = ptr.add(1);
                if (*ptr).is_none() {
                    return Some(idx);
                }
            }
        }

        self.find_first_free(0)
    }

    /// Clear the allocations by dropping all possible inserted objects and resetting
    /// indexes
    pub fn clear(&mut self) {
        unsafe {
            // Check and drop contents if possible
            for idx in 0..self.capacity {
                let ptr = self.buf_ptr.as_ptr().add(idx);
                if mem::needs_drop::<T>() {
                    std::ptr::drop_in_place(ptr);
                }
                std::ptr::write(self.buf_ptr.as_ptr().add(idx), None);
            }
            self.len = 0;
            self.next_free = 0;
        }
    }
}

#[cfg(test)]
mod tests {
    use super::Lump;
    use std::mem::size_of;

    #[test]
    fn struct_size() {
        struct A {
            _name: String,
            _state: String,
        }

        assert_eq!(size_of::<usize>(), 8);
        assert_eq!(size_of::<Lump<u8>>() / size_of::<u8>(), 32);
        assert_eq!(size_of::<Lump<A>>() / size_of::<usize>(), 4);
    }

    #[test]
    fn alloc() {
        struct A {
            _name: String,
            _state: String,
        }
        let _tmp: Lump<A> = Lump::new(100000);
    }

    #[test]
    fn push_10() {
        let count = 10;
        let mut lump = Lump::new(count);
        for i in 0..count {
            lump.push(i);
        }
        assert_eq!(lump.len(), count);
    }

    #[test]
    fn push_100000000() {
        let count = 100000000;
        let mut lump = Lump::new(count);
        for i in 0..count {
            lump.push(i);
        }
        assert_eq!(lump.len(), count);
    }

    #[test]
    fn insert_remove_to_zero() {
        let count = 10000;
        let mut lump = Lump::new(count);
        for i in 0..count {
            lump.push(i);
        }
        assert_eq!(lump.len(), count);

        for i in 0..count {
            lump.remove(i);
        }
        assert_eq!(lump.len(), 0);
    }

    #[test]
    fn insert_and_remove_all_insert() {
        let mut links = Lump::new(64);

        let idx1 = links.push(42).unwrap();
        let idx2 = links.push(666).unwrap();
        let idx3 = links.push(123).unwrap();
        let idx4 = links.push(69).unwrap();
        links.remove(idx1);
        links.remove(idx2);
        links.remove(idx3);
        links.remove(idx4);

        assert_eq!(links.len(), 0);

        let idx1 = links.push(42).unwrap();
        assert_eq!(links.get(idx1), Some(&42));
    }

    #[test]
    fn insert_get_mut() {
        let mut links = Lump::new(64);

        let idx1 = links.push(42).unwrap();
        let idx2 = links.push(666).unwrap();
        let idx3 = links.push(123).unwrap();

        assert_eq!(links.get(idx1), Some(&42));
        assert_eq!(links.get(idx2), Some(&666));
        assert_eq!(links.get(idx3), Some(&123));

        if let Some(dat) = links.get_mut(idx2) {
            *dat = 333;
        }
        assert_eq!(links.get(idx2), Some(&333));
    }

    #[test]
    fn partial_fill_and_clear() {
        let count = 10000;
        let mut lump = Lump::new(count);
        for i in (0..count).step_by(2) {
            lump.push(i);
        }
        assert_eq!(lump.len(), count / 2);

        lump.clear();
        assert_eq!(lump.len(), 0);
        assert_eq!(lump.get(5), None);
    }

    #[test]
    fn partial_fill_and_drop() {
        let count = 10000;
        let mut lump = Lump::new(count);
        for i in (0..count).step_by(2) {
            lump.push(i);
        }
        assert_eq!(lump.len(), count / 2);

        std::mem::drop(lump);
    }

    #[test]
    fn removing() {
        let count = 128;
        let mut lump = Lump::new(count);
        for i in 0..count {
            lump.push(i);
        }
        assert_eq!(lump.len(), count);

        lump.remove(64);
        assert_eq!(lump.len(), count - 1);
        assert_eq!(lump.get(64), None);

        lump.push(64);
        assert_eq!(lump.len(), count);
    }
}
