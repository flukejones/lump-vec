use std::{marker::PhantomData, ptr::NonNull};

use crate::Lump;

pub struct IterOptioned<'a, T> {
    max: usize,
    next_idx: usize,
    pool: &'a Lump<T>,
}

impl<'a, T> IterOptioned<'a, T> {
    pub(crate) fn new(max: usize, pool: &'a Lump<T>) -> Self {
        Self {
            max,
            next_idx: 0,
            pool,
        }
    }
}

impl<'a, T> Iterator for IterOptioned<'a, T> {
    type Item = &'a T;

    // next() is the only required method
    fn next(&mut self) -> Option<Self::Item> {
        if self.next_idx > self.max {
            return None;
        }
        for idx in self.next_idx..self.pool.capacity {
            if self.pool.get(idx).is_some() {
                self.next_idx = idx + 1;
                return self.pool.get(idx);
            }
        }
        None
    }
}

impl<T> Lump<T> {
    pub fn iter(&self) -> IterOptioned<T> {
        IterOptioned::new(self.len, self)
    }
}

pub struct IterOptionedMut<'a, T> {
    max: usize,
    next_idx: usize,
    pool: NonNull<Lump<T>>,
    _phantom: PhantomData<&'a mut Lump<T>>,
}

impl<'a, T> IterOptionedMut<'a, T> {
    pub(crate) fn new(max: usize, pool: &'a mut Lump<T>) -> Self {
        Self {
            max,
            next_idx: 0,
            pool: NonNull::from(pool),
            _phantom: PhantomData,
        }
    }
}

impl<'a, T> Iterator for IterOptionedMut<'a, T> {
    type Item = &'a mut T;

    // next() is the only required method
    fn next(&mut self) -> Option<Self::Item> {
        if self.next_idx > self.max {
            return None;
        }
        unsafe {
            for idx in self.next_idx..self.pool.as_ref().capacity {
                if self.pool.as_ref().get(idx).is_some() {
                    self.next_idx = idx + 1;
                    return self.pool.as_mut().get_mut(idx);
                }
            }
        }
        None
    }
}

impl<T> Lump<T> {
    pub fn iter_mut(&mut self) -> IterOptionedMut<T> {
        IterOptionedMut::new(self.len, self)
    }
}

#[cfg(test)]
mod tests {
    use crate::Lump;

    #[test]
    fn pool_iter_and_map() {
        let mut pool = Lump::new(64);

        pool.push(42);
        pool.push(123);
        pool.push(666);
        pool.push(333);
        assert_eq!(pool.len(), 4);

        for (i, num) in pool.iter().enumerate() {
            if i == 0 {
                assert_eq!(*num, 42);
            }
            if i == 2 {
                assert_eq!(*num, 666);
            }
        }

        assert_eq!(pool.iter().copied().count(), 4);

        pool.remove(2);
        assert_eq!(pool.iter().copied().count(), 3);
    }

    #[test]
    fn pool_iter_mutate() {
        let mut pool = Lump::new(64);

        pool.push(42);
        pool.push(123);
        pool.push(666);
        pool.push(333);
        assert_eq!(pool.len(), 4);
        assert_eq!(pool.get(1), Some(&123));
        assert_eq!(pool.get(3), Some(&333));

        for n in pool.iter_mut() {
            *n = 1;
        }
        assert_eq!(pool.get(3), Some(&1));
        pool.remove(2);
        assert_eq!(pool.get(2), None);
        assert_eq!(pool.get(3), Some(&1));

        for i in pool.iter_mut() {
            assert_eq!(*i, 1);
        }

        let c: Vec<i32> = pool.iter_mut().map(|n| *n).collect();
        assert_eq!(c.len(), 3);
        assert_eq!(c.get(2), Some(&1));
    }
}
