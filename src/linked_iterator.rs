use crate::linked_lump::LinkNode;
use std::{marker::PhantomData, ptr::null_mut};

use crate::LinkedLump;

pub struct IterLinks<'a, T> {
    start: *mut LinkNode<T>,
    current: *mut LinkNode<T>,
    _phantom: PhantomData<&'a T>,
}

impl<'a, T> IterLinks<'a, T> {
    pub(crate) fn new(start: *mut LinkNode<T>) -> Self {
        Self {
            start,
            current: null_mut(),
            _phantom: PhantomData,
        }
    }
}

impl<'a, T> Iterator for IterLinks<'a, T> {
    type Item = &'a T;

    fn next(&mut self) -> Option<Self::Item> {
        unsafe {
            if !self.current.is_null() && self.current == self.start {
                return None;
            } else if self.current.is_null() {
                self.current = self.start;
            }

            let current = self.current;
            self.current = (*self.current).next;
            Some(&(*current).data)
        }
    }
}

impl<T> LinkedLump<T> {
    pub fn iter(&self) -> IterLinks<T> {
        IterLinks::new(self.1)
    }
}

pub struct IterLinksMut<'a, T> {
    start: *mut LinkNode<T>,
    current: *mut LinkNode<T>,
    _phantom: PhantomData<&'a T>,
}

impl<'a, T> IterLinksMut<'a, T> {
    pub(crate) fn new(start: *mut LinkNode<T>) -> Self {
        Self {
            start,
            current: null_mut(),
            _phantom: PhantomData,
        }
    }
}

impl<'a, T> Iterator for IterLinksMut<'a, T> {
    type Item = &'a mut T;

    fn next(&mut self) -> Option<Self::Item> {
        unsafe {
            if !self.current.is_null() && self.current == self.start {
                return None;
            } else if self.current.is_null() {
                self.current = self.start;
            }

            let current = self.current;
            self.current = (*self.current).next;
            Some(&mut (*current).data)
        }
    }
}

impl<T> LinkedLump<T> {
    pub fn iter_mut(&mut self) -> IterLinksMut<T> {
        IterLinksMut::new(self.1)
    }
}

#[cfg(test)]
mod tests {
    use crate::LinkedLump;

    #[test]
    fn link_iter_and_map() {
        let mut links = LinkedLump::new(64);

        links.push(42);
        links.push(123);
        links.push(666);
        links.push(333);

        for (i, num) in links.iter().enumerate() {
            if i == 0 {
                assert_eq!(*num, 42);
            }
            if i == 2 {
                assert_eq!(*num, 666);
            }
        }

        assert_eq!(links.iter().copied().count(), 4);
    }

    #[test]
    fn link_iter_mut_and_map() {
        let mut links = LinkedLump::new(64);

        links.push(42);
        links.push(123);
        links.push(666);
        links.push(333);

        for (i, num) in links.iter_mut().enumerate() {
            if i == 0 {
                assert_eq!(*num, 42);
            }
            if i == 2 {
                assert_eq!(*num, 666);
            }
            *num = 1;
        }

        let c: Vec<i32> = links.iter_mut().map(|n| *n).collect();
        assert_eq!(c.len(), 4);

        for i in c {
            assert_eq!(i, 1);
        }
    }

    #[test]
    fn link_iter_mutate() {
        let mut links = LinkedLump::new(64);

        links.push(42);
        links.push(123);
        links.push(666);
        links.push(333);

        assert_eq!(links.len(), 4);
        assert_eq!(links.get(1), Some(&123));
        assert_eq!(links.get(3), Some(&333));

        for n in links.iter_mut() {
            *n = 1;
        }
        assert_eq!(links.get(3), Some(&1));
        links.remove(2);
        assert_eq!(links.get(2), None);
        assert_eq!(links.get(3), Some(&1));

        for i in links.iter_mut() {
            assert_eq!(*i, 1);
        }

        let c: Vec<i32> = links.iter_mut().map(|n| *n).collect();
        assert_eq!(c.len(), 3);
        assert_eq!(c.get(2), Some(&1));
    }
}
