//! The main purpose of `linked` is to provide a wrapper around the allocator
//! which inserts `<T>` as `LinkNode<T>` to provide a fast way of iterating
//! over entries.

pub use crate::linked_iterator::{IterLinks, IterLinksMut};
use crate::Lump;
use std::ptr::{NonNull, null_mut};

/// Create a variation of `Lump` where `T` is wrapped in a linked-list node.
pub struct LinkedLump<T>(pub(crate) Lump<LinkNode<T>>, pub(crate) *mut LinkNode<T>);

impl<T> LinkedLump<T> {
    /// Make a new Linked where objects are wrapped in linked-list nodes. The `capacity`
    /// is the total amount of objects you can push - once this limit is reached
    /// a push will return `None` until you remove items.
    ///
    /// There is no method of reallocation to increase size.
    pub fn new(capacity: usize) -> Self {
        Self(Lump::new(capacity), null_mut())
    }

    /// Get the actual used length of this Linked. This is the amount of items using
    /// space in the Linked.
    pub const fn len(&self) -> usize {
        self.0.len
    }

    pub const fn is_empty(&self) -> bool {
        self.0.len == 0
    }

    /// Get the allocated capacity of this Linked. This is how many items you can
    /// add in toatl.
    pub const fn capacity(&self) -> usize {
        self.0.capacity
    }

    /// Get a reference to the item at this index. Returns `None` if there is no
    /// item at the index.
    pub fn get(&self, idx: usize) -> Option<&T> {
        if let Some(node) = self.0.get(idx) {
            return Some(&node.data);
        }
        None
    }

    /// Get a mutable reference to the item at this index. Returns `None` if there is no
    /// item at the index.
    pub fn get_mut(&mut self, idx: usize) -> Option<&mut T> {
        if let Some(node) = self.0.get_mut(idx) {
            return Some(&mut node.data);
        }
        None
    }

    /// Get an unchecked reference to the item at this index. It is undefined behaviour if:
    /// the index is not valid, out of bounds, the item is not allocated, was
    /// previously removed etc.
    ///
    /// # Safety
    /// For this method to be safe the index requested must be live, meaning that
    /// an item was pushed to the Linked, not removed, and the index returned is what
    /// is being used here. As items are not zeroed on removal it is possible to
    /// get valid data for `T` back, but there it then becomes UB if used.
    pub unsafe fn get_unchecked(&self, idx: usize) -> &T {
        &self.0.get(idx).unwrap_unchecked().data
    }

    /// Get an unchecked mutable reference to the item at this index. It is undefined behaviour if:
    /// the index is not valid, out of bounds, the item is not allocated, was
    /// previously removed etc.
    ///
    /// # Safety
    /// For this method to be safe the index requested must be live, meaning that
    /// an item was pushed to the Linked, not removed, and the index returned is what
    /// is being used here. As items are not zeroed on removal it is possible to
    /// get valid data for `T` back, but there it then becomes UB if used.
    pub unsafe fn get_mut_unchecked(&mut self, idx: usize) -> &mut T {
        &mut self.0.get_mut(idx).unwrap_unchecked().data
    }

    /// Push an item to the Linked. Returns the index the item was pushed to if
    /// successful. This index can be used to remove the item, if you want to
    /// accurately remove the pushed item you should store this somewhere.
    pub fn push(&mut self, obj: T) -> Option<usize> {
        let obj = LinkNode::new(obj);
        if let Some(idx) = self.0.push(obj) {
            let new_head = unsafe { self.0.get_mut(idx).unwrap_unchecked() };

            if self.1.is_null() {
                self.1 = new_head as *mut LinkNode<T>;
                new_head.prev = new_head as *mut LinkNode<T>;
                new_head.next = new_head as *mut LinkNode<T>;
            } else {
                unsafe {
                    let head = self.1;
                    new_head.prev = head;
                    new_head.next = (*head).next;
                    (*(*head).next).prev = new_head as *mut LinkNode<T>;
                    (*head).next = new_head as *mut LinkNode<T>;
                    self.1 = new_head as *mut LinkNode<T>;
                }
            }
            return Some(idx);
        }
        None
    }

    /// Ensure head is null if the pool is zero length
    fn maybe_reset_head(&mut self) {
        if self.0.len == 0 {
            self.1 = null_mut();
        }
    }

    /// Removes the entry at index
    pub fn remove(&mut self, idx: usize) {
        // Need to take so that neighbour nodes can be updated
        self.take(idx);
        self.maybe_reset_head();
    }

    /// Takes the `T` at index leaving nothing in its place.
    pub fn take(&mut self, idx: usize) -> Option<T> {
        unsafe {
            let ptr = self.0.buf_ptr.as_ptr().add(idx);
            let inner_ptr = (*ptr).as_mut()? as *mut LinkNode<T>;
            if inner_ptr == self.1
            {
                self.1 = (*self.1).prev;
            }
        }
        if let Some(node) = self.0.take(idx) {
            unsafe {
                (*node.next).prev = node.prev;
                (*node.prev).next = node.next;
            }

            self.maybe_reset_head();
            return Some(node.data);
        }
        None
    }

    /// Clear the allocations by dropping all possible inserted objects and reseting
    /// indexes
    pub fn clear(&mut self) {
        self.0.clear();
        self.maybe_reset_head();
    }
}

pub(crate) struct LinkNode<T> {
    pub(crate) prev: *mut LinkNode<T>,
    pub(crate) next: *mut LinkNode<T>,
    pub(crate) data: T,
}

impl<T> LinkNode<T> {
    fn new(data: T) -> Self {
        Self {
            prev: null_mut(),
            next: null_mut(),
            data,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::LinkedLump;
    use std::mem::size_of;

    #[test]
    fn struct_size() {
        struct A {
            _name: String,
            _state: String,
        }

        assert_eq!(size_of::<usize>(), 8);
        assert_eq!(size_of::<LinkedLump<u8>>() / size_of::<u8>(), 40);
        assert_eq!(size_of::<LinkedLump<A>>() / size_of::<usize>(), 5);
    }

    #[test]
    fn check_next_prev_links() {
        let mut links = LinkedLump::new(64);

        links.push(42);
        assert!(!links.1.is_null());

        let idx = links.push(666).unwrap();
        unsafe {
            assert_eq!((*(*links.1).next).data, 42);
            assert_eq!((*(*(*links.1).next).next).data, 666);
        }
        links.push(123);

        unsafe {
            assert_eq!((*(*(*links.1).next).next).data, 666);
            assert_eq!((*(*(*(*links.1).next).next).next).data, 123);
        }

        links.remove(idx);
        unsafe {
            assert_eq!((*(*links.1).next).data, 42);
            assert_eq!((*(*(*links.1).next).next).data, 123);
        }
    }

    #[test]
    fn insert_remove_to_zero() {
        let count = 10000;
        let mut lump = LinkedLump::new(count);
        for i in 0..count {
            lump.push(i);
        }
        assert_eq!(lump.len(), count);

        for i in 0..count {
            lump.remove(i);
        }
        assert_eq!(lump.len(), 0);
    }

    #[test]
    fn push_100000000() {
        let count = 100000000;
        let mut lump = LinkedLump::new(count);
        for i in 0..count {
            lump.push(i);
        }

        assert_eq!(lump.len(), count);

        lump.push(10);
        assert_eq!(lump.len(), count);
    }

    #[test]
    fn insert_and_remove() {
        let mut links = LinkedLump::new(64);

        links.push(42);
        let idx = links.push(666).unwrap();
        let idx2 = links.push(123).unwrap();
        let idx3 = links.push(69).unwrap();

        links.remove(idx);
        unsafe {
            assert_eq!((*(*links.1).next).data, 42);
            assert_eq!((*(*(*links.1).next).prev).data, 69);
            assert_eq!((*(*(*links.1).next).next).data, 123);
        }

        links.remove(idx2);
        unsafe {
            assert_eq!((*(*links.1).next).data, 42);
            assert_eq!((*links.1).data, 69);
        }

        links.remove(idx3);
        unsafe {
            assert_eq!((*links.1).data, 42);
        }
    }

    #[test]
    fn insert_and_remove_all_insert() {
        let mut links = LinkedLump::new(64);

        let idx1 = links.push(42).unwrap();
        let idx2 = links.push(666).unwrap();
        let idx3 = links.push(123).unwrap();
        let idx4 = links.push(69).unwrap();
        links.remove(idx1);
        links.remove(idx2);
        links.remove(idx3);
        links.remove(idx4);

        assert_eq!(links.len(), 0);
        assert!(links.1.is_null());

        let idx1 = links.push(42).unwrap();
        assert!(!links.1.is_null());
        assert_eq!(links.get(idx1), Some(&42));
    }

    #[test]
    fn insert_get_mut() {
        let mut links = LinkedLump::new(64);

        let idx1 = links.push(42).unwrap();
        let idx2 = links.push(666).unwrap();
        let idx3 = links.push(123).unwrap();

        assert_eq!(links.get(idx1), Some(&42));
        assert_eq!(links.get(idx2), Some(&666));
        assert_eq!(links.get(idx3), Some(&123));

        if let Some(dat) = links.get_mut(idx2) {
            *dat = 333;
        }
        assert_eq!(links.get(idx2), Some(&333));
    }

    #[test]
    fn partial_fill_and_clear() {
        let count = 10000;
        let mut lump = LinkedLump::new(count);
        for i in (0..count).step_by(2) {
            lump.push(i);
        }
        assert_eq!(lump.len(), count / 2);

        unsafe {
            assert_eq!(*lump.get_unchecked(2), 4);
        }

        lump.clear();
        assert_eq!(lump.len(), 0);
    }

    #[test]
    fn partial_fill_and_drop() {
        let count = 10000;
        let mut lump = LinkedLump::new(count);
        for i in (0..count).step_by(2) {
            lump.push(i);
        }
        assert_eq!(lump.len(), count / 2);

        std::mem::drop(lump);
    }

    #[test]
    fn removing() {
        let count = 128;
        let mut lump = LinkedLump::new(count);
        for i in 0..count {
            lump.push(i);
        }
        assert_eq!(lump.len(), count);

        lump.remove(64);
        assert_eq!(lump.len(), count - 1);

        assert_eq!(lump.get(64), None);

        lump.push(64);
        lump.remove(64);
        assert_eq!(lump.len(), count - 1);

        // Should now be zero
        unsafe {
            assert_eq!(*lump.get_unchecked(65), 65); // others are cool
        }
    }

    #[test]
    fn yeeting_of_the_t() {
        let count = 128;
        let mut lump = LinkedLump::new(count);
        for i in 0..count {
            lump.push(i);
        }
        assert_eq!(lump.len(), count);

        let tmp = lump.take(64);
        assert_eq!(tmp, Some(64));
        assert_eq!(lump.len(), count - 1);

        lump.push(64);
        let tmp = lump.take(64);
        assert_eq!(tmp, Some(64));
        assert_eq!(lump.len(), count - 1);

        // Should now be zero
        unsafe {
            assert_eq!(*lump.get_unchecked(65), 65); // others are cool
        }
    }
}
