use lump_vec::LinkedLump;

#[derive(Debug, PartialEq, PartialOrd, Clone, Copy)]
pub enum ThinkerState {
    Active,
    Removed,
}

struct A {
    name: String,
    state: ThinkerState,
}

impl A {
    fn new(name: &str) -> Self {
        Self {
            name: name.to_string(),
            state: ThinkerState::Active,
        }
    }
}

impl Think for A {
    fn think(&mut self) -> bool {
        dbg!("{} thinking...", &self.name);
        true
    }

    fn state(&self) -> ThinkerState {
        self.state
    }
}

struct B {
    name: String,
    state: ThinkerState,
}

impl B {
    fn new(name: &str) -> Self {
        Self {
            name: name.to_string(),
            state: ThinkerState::Active,
        }
    }
}

impl Think for B {
    fn think(&mut self) -> bool {
        dbg!("{} thinking...", &self.name);
        true
    }

    fn state(&self) -> ThinkerState {
        self.state
    }
}

pub trait Think {
    /// impl of this trait should return true *if* the thinker + object are to be removed
    fn think(&mut self) -> bool;

    fn as_ptr(&mut self) -> *mut dyn Think
    where
        Self: 'static + Sized,
    {
        self as *mut dyn Think
    }

    fn state(&self) -> ThinkerState;
}

fn main() {
    let mut pool = LinkedLump::<Box<dyn Think>>::new(10);

    let idx1 = pool.push(Box::new(A::new("struct A1"))).unwrap();
    assert_eq!(idx1, 0);
    let idx2 = pool.push(Box::new(B::new("struct B1"))).unwrap();
    assert_eq!(idx2, 1);
    let idx3 = pool.push(Box::new(A::new("struct A2"))).unwrap();
    assert_eq!(idx3, 2);
    let idx4 = pool.push(Box::new(B::new("struct B2"))).unwrap();
    assert_eq!(idx4, 3);
    let idx5 = pool.push(Box::new(B::new("struct B3"))).unwrap();
    assert_eq!(idx5, 4);
    let idx6 = pool.push(Box::new(A::new("struct A3"))).unwrap();
    assert_eq!(idx6, 5);

    for thinker in pool.iter_mut() {
        thinker.think();
    }

    assert_eq!(pool.len(), 6);
    pool.remove(idx2);
    assert_eq!(pool.len(), 5);

    // test compile error
    // let tmp = pool.get(idx5).unwrap();
    for thinker in pool.iter_mut() {
        thinker.think();
    }
    // tmp.think();

    for thinker in pool.iter() {
        assert_eq!(thinker.state(), ThinkerState::Active);
    }
}
