# Lump Vec

I initially was using a bitmap mask type of thing to track allocation slot
use. But after benchmarking between that and using plain `Option` I decided
to drop it. Results:

```
Push 100,000/Bitmap slot tracking                                                                            
                        time:   [463.05 us 463.27 us 463.53 us]

Push 100,000/Option slot tracking                                                                            
                        time:   [79.043 us 79.183 us 79.348 us]

Push 100,000/Linked (bitmap) slot                                                                            
                        time:   [686.49 us 686.71 us 686.94 us]

Push 100,000/Linked (option) slot                                                                            
                        time:   [159.91 us 160.52 us 161.25 us]
```

Documentation is in progress. Features are maybe not complete.
