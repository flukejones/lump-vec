use criterion::*;
use lump_vec::{LinkedLump, Lump};

struct TestObject {
    pub x: u32,
}

fn bare_100_000(b: &mut Bencher) {
    b.iter(|| {
        let mut lump = Lump::new(100000);

        for i in 0..100000 {
            lump.push(TestObject { x: i });
        }
    });
}

fn linked_100_000(b: &mut Bencher) {
    b.iter(|| {
        let mut lump = LinkedLump::new(100000);

        for i in 0..100000 {
            lump.push(TestObject { x: i });
        }
    });
}

fn bare_100_000_scatter(b: &mut Bencher) {
    let mut lump = Lump::new(100000);

    for i in 0..100000 {
        lump.push(TestObject { x: i });
    }

    let mut count = 0;
    for i in (0..100000).into_iter().step_by(4) {
        lump.remove(i);
        count += 1;
    }

    b.iter(|| {
        for i in 0..count {
            lump.push(TestObject { x: i });
        }
    });
}

fn linked_100_000_scatter(b: &mut Bencher) {
    let mut lump = LinkedLump::new(100000);

    for i in 0..100000 {
        lump.push(TestObject { x: i });
    }

    let mut count = 0;
    for i in (0..100000).into_iter().step_by(4) {
        lump.remove(i);
        count += 1;
    }

    b.iter(|| {
        for i in 0..count {
            lump.push(TestObject { x: i });
        }
    });
}

fn bench(c: &mut Criterion) {
    let mut group = c.benchmark_group("Push 100,000");

    group.bench_function("Lump slot", bare_100_000);
    group.bench_function("LinkedLump slot", linked_100_000);
    group.bench_function("Lump scatter push", bare_100_000_scatter);
    group.bench_function("LinkedLump scatter push", linked_100_000_scatter);
}

criterion_group!(benches, bench,);
criterion_main!(benches);
